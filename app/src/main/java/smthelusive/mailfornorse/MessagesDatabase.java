package smthelusive.mailfornorse;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MessagesDatabase extends SQLiteOpenHelper {

    public MessagesDatabase(Context context) {
        super(context, "Norse", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table threads ("
                + "id text ,"
                + "name text" + ");");
        db.execSQL("create table messages ("
                + "threadId text, "
                + "message text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS messages");
        db.execSQL("DROP TABLE IF EXISTS threads");
        onCreate(db);
    }
}