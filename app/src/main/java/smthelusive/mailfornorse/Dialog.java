package smthelusive.mailfornorse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import com.google.api.services.gmail.model.*;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.Thread;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.os.Bundle;

public class Dialog extends Activity {
    ProgressDialog mProgress;

    private static String threadId;
    private LinearLayout ll;
    private List<String> messages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        Intent intent = getIntent();
        threadId = intent.getStringExtra("dialog_id");
        if (isDeviceOnline()) {
            All.messagesDatabase = new MessagesDatabase(this);
            All.db = All.messagesDatabase.getWritableDatabase();
            All.db.delete("messages", "threadId = '" + threadId + "'", null);
            All.messagesDatabase.close();
        }
        ll = (LinearLayout)findViewById(R.id.messageList);
        mProgress = new ProgressDialog(this);
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);
        mProgress.setMessage("Loading...");
    }

    public void addSmbMessageToView(String message, boolean my) {
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.weight = 1;
        TextView tv = new TextView(this);
        tv.setText(message);
        LinearLayout gl = new LinearLayout(this);
        gl.setOrientation(LinearLayout.HORIZONTAL);
        gl.setWeightSum(2);
        if (my) {
            TextView space = new TextView(this);
            space.setText("\t\t\t");
            space.setLayoutParams(lp);
            gl.addView(space);
        }
        tv.setLayoutParams(lp);
        tv.setBackgroundColor(Color.GRAY);
        tv.setTextColor(Color.BLUE);
        tv.setTextSize(24);
        gl.addView(tv);
        gl.setPadding(0, 0, 0, 20);
        ll.addView(gl);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isDeviceOnline() && isGooglePlayServicesAvailable()) {
            try {
                refreshResults();
            } catch (Exception e) {
                //don't mind if it is not properly loaded
            }
        } else {
            showOffline();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case All.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case All.REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        All.mCredential.setSelectedAccountName(accountName);
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(All.PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(Dialog.this, "Account unspecified.",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case All.REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void refreshResults() {
        if (All.mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else {
            new MakeRequestTask(All.mCredential).execute();
        }
    }

    private void showOffline() {
        if (All.messages != null)
            messages = All.messages.get(threadId);
        if (messages != null)
        for (String m : messages) {
            if (m.charAt(m.length()-1) == '1') {
                addSmbMessageToView(m.substring(0, m.length()-1), true);
            }
            else addSmbMessageToView(m.substring(0, m.length()-1), false);
        }
    }

    private void writeMessageToDb(String message) {
        All.messagesDatabase = new MessagesDatabase(Dialog.this);
        All.db = All.messagesDatabase.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("message", message);
        cv.put("threadId", threadId);
        All.db.insert("messages", null, cv);
        All.messagesDatabase.close();

    }
    private void chooseAccount() {
        startActivityForResult(
                All.mCredential.newChooseAccountIntent(), All.REQUEST_ACCOUNT_PICKER);
    }

    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS ) {
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        android.app.Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode,
                Dialog.this,
                All.REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, Void> {
        private com.google.api.services.gmail.Gmail mService = null;
        private Exception mLastError = null;

        public MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.gmail.Gmail.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("MailForNorse")
                    .build();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        @SuppressWarnings("unchecked")
        private Void getDataFromApi() throws IOException {
            messages = new ArrayList<>();
            String user = "me";
            Thread thread = mService.users().threads().get(user, threadId).execute();
            List<Message> threadMessages = thread.getMessages();
            String result;
            Message m;
            int i = 0;
            String from = "";
            if (threadMessages != null && threadMessages.size() > 0) {
                All.messages = new HashMap<>();
                while (i < threadMessages.size()) {
                    result = "";
                    m = mService.users().messages().get(user, threadMessages.get(i).getId()).execute();
                    List<String> labelsToRemove = new ArrayList<>();
                    //mark opened as read:
                    labelsToRemove.add("UNREAD");
                    ModifyMessageRequest mods = new ModifyMessageRequest().setRemoveLabelIds(labelsToRemove);
                    mService.users().messages().modify(user, m.getId(), mods).execute();

                    List<MessagePartHeader> headers = m.getPayload().getHeaders();
                    for (MessagePartHeader header : headers) {
                        if (header.getName().toLowerCase().equals("from")) {
                            from = header.getValue();
                            result += from.split("<")[0];
                        }
                    }
                    result += "\n" + (m.getSnippet().length() > 20 ? m.getSnippet().substring(0, 20) + "..." : m.getSnippet()) +
                            (from.contains(All.mCredential.getSelectedAccountName()) ? "1"/*from me*/ : "0"/*from dmb else*/);
                    messages.add(result);
                    writeMessageToDb(result);
                    if (++i == 9) break;
                }
                All.messages.put(threadId, messages);
            }
            All.messagesDatabase.close();
            return null;
        }


        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(Void output) {
            mProgress.hide();
            if (messages == null || messages.size() == 0) {
                messages = All.messages.get(threadId);
            }
            for (String m : messages) {
                if (m.charAt(m.length()-1) == '1') {
                    addSmbMessageToView(m.substring(0, m.length()-1), true);
                }
                else addSmbMessageToView(m.substring(0, m.length()-1), false);
            }
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            All.REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(Dialog.this, "The following error occurred:\n" + mLastError.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(Dialog.this, "Request cancelled.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
