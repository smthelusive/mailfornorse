package smthelusive.mailfornorse;

import android.database.sqlite.SQLiteDatabase;

import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.services.gmail.GmailScopes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class All {
    public static final int REQUEST_ACCOUNT_PICKER = 1000;
    public static final int REQUEST_AUTHORIZATION = 1001;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    public static final String PREF_ACCOUNT_NAME = "accountName";
    public static final String[] SCOPES = { GmailScopes.GMAIL_LABELS, GmailScopes.GMAIL_READONLY, GmailScopes.GMAIL_MODIFY };
    public static GoogleAccountCredential mCredential;

    public static SQLiteDatabase db;
    public static MessagesDatabase messagesDatabase;
    //-----------
    public static HashMap<String, String> threads;
    public static List<String> threadIds;
    public static HashMap<String, List<String>> messages;
    public static ArrayList<Boolean> labels;

}
