package smthelusive.mailfornorse;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;

import com.google.api.services.gmail.model.*;
import com.google.api.services.gmail.model.Message;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.*;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

    ProgressDialog mProgress;
    private LinearLayout ll;
    private String tmpId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        ll = (LinearLayout)findViewById(R.id.maillistLayout);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Loading...");
        mProgress.setCancelable(false);
        mProgress.setCanceledOnTouchOutside(false);

        All.messagesDatabase = new MessagesDatabase(this);
        All.db = All.messagesDatabase.getWritableDatabase();
        if (isDeviceOnline()) {
            All.db.delete("threads", null, null);
            All.messagesDatabase.close();
            SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
            All.mCredential = GoogleAccountCredential.usingOAuth2(
                    getApplicationContext(), Arrays.asList(All.SCOPES))
                    .setBackOff(new ExponentialBackOff())
                    .setSelectedAccountName(settings.getString(All.PREF_ACCOUNT_NAME, null));
        } else showOffline();
    }

    private void refreshListView(ArrayList<String> resource, List<Boolean> read) {
        if (resource != null) {
            if (ll.getChildCount() > 0)
                ll.removeAllViews();
            TextView tv;
            for (int i = 0; i < resource.size(); i++) {
                tv = new TextView(this);
                tv.setText(resource.get(i));
                tv.setTextColor(Color.BLUE);
                tv.setTextSize(24);
                tv.setGravity(Gravity.CENTER);
                if (read != null && !read.get(i)) {
                    SpannableString content = new SpannableString(resource.get(i));
                    content.setSpan(new StyleSpan(Typeface.BOLD), 0, content.length(), 0);
                    tv.setText(content);
                }
                tmpId = All.threadIds.get(i);
                tv.setTag(tmpId);
                tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, smthelusive.mailfornorse.Dialog.class);
                        intent.putExtra("dialog_id", (String)v.getTag());
                        startActivity(intent);
                    }
                });
                ll.addView(tv);
            }
        }
    }


    private void readThreadNames() throws SQLiteException{
        All.messagesDatabase = new MessagesDatabase(this);
        All.db = All.messagesDatabase.getWritableDatabase();
        String[] columns;
        Cursor c;
        columns = new String[] { "id", "name" };
        c = All.db.query("threads", columns, null, null, null, null, null);
        if (c.moveToFirst()) {
            int idColIndex = c.getColumnIndex("id");
            int nameColIndex = c.getColumnIndex("name");
            do {
                String value = c.getString(idColIndex);
                All.threadIds.add(value);
                All.threads.put(value, c.getString(nameColIndex));
            } while (c.moveToNext());
            c.close();
        }
        All.messagesDatabase.close();
    }
    private void readMessageScopes() throws SQLiteException {
        All.messagesDatabase = new MessagesDatabase(this);
        All.db = All.messagesDatabase.getWritableDatabase();
        String[] columns;
        Cursor c;
        String selection;
        columns = new String[] { "message" };
        List<String> messageScope;
        for (int i = 0; i < All.threadIds.size(); i++) {
            messageScope = new ArrayList<>();
            selection = "threadId = '" + All.threadIds.get(i) + "'";
            c = All.db.query("messages", columns, selection, null, null, null, null);
            if (c.moveToFirst()) {
                int messageColIndex = c.getColumnIndex("message");
                do {
                    messageScope.add(c.getString(messageColIndex));
                } while (c.moveToNext());
                c.close();
            }
            All.messages.put(All.threadIds.get(i), messageScope);
        }
        All.messagesDatabase.close();
    }
    private void readAllToApp() {
        All.messages = new HashMap<>();
        All.threads = new HashMap<>();
        All.threadIds = new ArrayList<>();
        try {
            readThreadNames();
            readMessageScopes();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (isDeviceOnline() && isGooglePlayServicesAvailable()) {
            try {
                refreshResults();
            } catch (Exception e) {
                //don't mind if it is not properly loaded
            }
        } else {
            showOffline();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case All.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case All.REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        All.mCredential.setSelectedAccountName(accountName);
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(All.PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(MainActivity.this, "Account unspecified.",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case All.REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void refreshResults() {
        if (All.mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else {
            new MakeRequestTask(All.mCredential).execute();
        }
    }
    @SuppressWarnings("unchecked")
    private void showOffline() {
        readAllToApp();
        List<String> threadNames = new ArrayList<String>();
        if (All.threadIds != null && All.threads != null) {
            for (String threadId : All.threadIds) {
                threadNames.add(All.threads.get(threadId));
            }
            refreshListView((ArrayList) threadNames, All.labels);
        }

    }
    private void chooseAccount() {
        startActivityForResult(
                All.mCredential.newChooseAccountIntent(), All.REQUEST_ACCOUNT_PICKER);
    }

    private void updateThreadsInDb() {
        All.messagesDatabase = new MessagesDatabase(MainActivity.this);
        All.db = All.messagesDatabase.getWritableDatabase();
        All.db.delete("threads", null, null);
        ContentValues cv;
        for (int i = 0; i < All.threadIds.size(); i++) {
            cv = new ContentValues();
            cv.put("id", All.threadIds.get(i));
            cv.put("name", All.threads.get(All.threadIds.get(i)));
            All.db.insert("threads", null, cv);
        }
        All.messagesDatabase.close();
    }
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS ) {
            return false;
        }
        return true;
    }

    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode,
                MainActivity.this,
                All.REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    private class MakeRequestTask extends AsyncTask<Void, Void, Void> {
        private com.google.api.services.gmail.Gmail mService = null;
        private Exception mLastError = null;

        public MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.gmail.Gmail.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("MailForNorse")
                    .build();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        private Void getDataFromApi() throws IOException {
            String user = "me";
            All.labels = new ArrayList<>();
            ListMessagesResponse messages = mService.users().messages().list(user).execute();
            if (messages != null && messages.size() > 0) {
                All.threads = new HashMap<>();
                All.threadIds = new ArrayList<>();
                String result;
                for (int i = 0; i < 10; i++) {
                    result = "";
                    Message m = mService.users().messages().get(user, messages.getMessages().get(i).getId()).execute();
                    List<MessagePartHeader> headers = m.getPayload().getHeaders();
                    for (MessagePartHeader header : headers) {
                        if (header.getName().toLowerCase().equals("from"))
                            result += header.getValue().split("<")[0];
                    }
                    All.threads.put(m.getThreadId(), result);
                    All.threadIds.add(m.getThreadId());
                    if (m.getLabelIds() != null && m.getLabelIds().contains("UNREAD"))
                        All.labels.add(false);
                    else All.labels.add(true);
                }
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void onPostExecute(Void v) {
            mProgress.hide();
            List<String> threadNames = new ArrayList<String>();
            if (All.threadIds != null && All.threads != null) {
                for (String threadId : All.threadIds) {
                    threadNames.add(All.threads.get(threadId));
                }
            }
            updateThreadsInDb();
            refreshListView((ArrayList)threadNames, All.labels);
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            All.REQUEST_AUTHORIZATION);
                } else {
                    Toast.makeText(MainActivity.this, "The following error occurred:\n" + mLastError.getMessage(),
                            Toast.LENGTH_LONG).show();
                    showOffline();
                }
            } else {
                Toast.makeText(MainActivity.this, "Request cancelled.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}